const fs = require("fs");
module.exports = {
  pluginOptions: {
    i18n: {
      locale: "en",
      fallbackLocale: "en",
      localeDir: "locales",
      enableInSFC: false,
    },
  },

  transpileDependencies: ["vuetify"],
  devServer: {
    https: {
      key: fs.readFileSync("./key.pem"),
      cert: fs.readFileSync("./cert.pem"),
    },
    public: "localhost:8080", // This is important for making sure your certificate matches the domain
  },
};
