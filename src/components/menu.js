export const menuItems = [
  {
    id: 1,
    label: "menuitems.menu.text",
    icon: "bx-home",
    link: "/",
    subItems: [
      {
        id: 2,
        label: "menuitems.menu.list.callOutManage",
        link: "/",
        parentId: 1,
      },
      {
        id: 3,
        label: "menuitems.menu.list.callInManage",
        link: "/thong-ke/bao-cao-cuoc-goi-vao",
        parentId: 1,
      },
    ],
  },
];
