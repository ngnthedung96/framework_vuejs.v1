import Vue from "vue";
import { BootstrapVue, IconsPlugin } from "bootstrap-vue";
import Vuelidate from "vuelidate";
import * as VueGoogleMaps from "vue2-google-maps";
import VueMask from "v-mask";
import VueRouter from "vue-router";
import vco from "v-click-outside";
import router from "./router/index";
import Scrollspy from "vue2-scrollspy";
import VueSweetalert2 from "vue-sweetalert2";
import store from "@/state/store";
import Multiselect from "vue-multiselect";
import DatePicker from "vue2-datepicker";
import moment from "moment";
import BootstrapVueTimeline from "bootstrap-vue-timeline";
import { BCard } from "bootstrap-vue";
import VueApexCharts from "vue-apexcharts";

Vue.component("b-card", BCard);
Vue.component("b-timeline", BootstrapVueTimeline);
Vue.component("apexchart", VueApexCharts);

// Import Bootstrap and BootstrapVue CSS files (order is important)
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";
import "vue-multiselect/dist/vue-multiselect.min.css";

// config facebook
import facebookService from "@/services/facebook.service";
facebookService();
// config axios

import { systemAxios } from "@/services/axios.service";
systemAxios.interceptors.request.use((config) => {
  const token = store.getters["authfack/token"];
  config.headers.Authorization = `Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MTc0MSwicGhvbmUiOiIwNDU2Nzg5MTAxIiwiaWF0IjoxNzA1MzkyNjQ3LCJleHAiOjE3MzY5NTAyNDd9.fOIS7NXs3kZFcv2luxtfGqgCXyx7m51OwErV5xAijEs`;
  return config;
});

// systemAxios.interceptors.response.use(
//   (res) => {
//     return res;
//   },
//   async (err) => {
//     const refreshToken = store.getters["authfack/refreshToken"];
//     const originalConfig = err.config;

//     if (
//       originalConfig.url !== "/auth/login" &&
//       originalConfig.url !== "/auth/refresh" &&
//       err.response
//     ) {
//       // Access Token was expired
//       if (err.response.status === 401 && !originalConfig._retry) {
//         originalConfig._retry = true;
//         try {
//           const rs = await systemAxios.post("/auth/refresh", {
//             refreshToken: refreshToken,
//           });

//           if (!rs.data.error) {
//             const data = rs.data.data;
//             store.dispatch("authfack/refreshToken", data);
//           }
//           return systemAxios(originalConfig);
//         } catch (_error) {
//           return Promise.reject(_error);
//         }
//       } else if (err.response.status === 401 && originalConfig._retry) {
//         store.dispatch("authfack/logout");
//       }
//     } else {
//       // console.log("url: ", err.response.status);
//       if (originalConfig.url !== "/auth/login") {
//         store.dispatch("authfack/logout");
//       }
//     }
//     return Promise.reject(err);
//   }
// );
Vue.prototype.$systemAxios = systemAxios;

// for notifying
import Snotify, { SnotifyPosition } from "vue-snotify";
// You also need to import the styles. If you're using webpack's css-loader, you can do so here:
import "vue-snotify/styles/material.css"; // or dark.css or simple.css
const options = {
  toast: {
    position: SnotifyPosition.rightTop,
  },
};
Vue.use(Snotify, options);

// socketio
import { io } from "socket.io-client";
import VueSocketIO from "vue-socket.io";
Vue.config.productionTip = false;
// const options = { path: '/my-app/' }; //Options object to pass into SocketIO

Vue.use(
  new VueSocketIO({
    debug: true,
    // connection: io("http://localhost:5555/"), //options object is Optional
    connection: io("https://facebook-api.topmove.vn/"), //options object is Optional
    vuex: {
      store,
      actionPrefix: "SOCKET_",
      mutationPrefix: "SOCKET_",
    },
  })
);

import "../src/design/app.scss";

import App from "./App.vue";

import { initFirebaseBackend } from "./authUtils";
import i18n from "./i18n";

import tinymce from "vue-tinymce-editor";

const firebaseConfig = {
  apiKey: process.env.VUE_APP_APIKEY,
  authDomain: process.env.VUE_APP_AUTHDOMAIN,
  databaseURL: process.env.VUE_APP_VUE_APP_DATABASEURL,
  projectId: process.env.VUE_APP_PROJECTId,
  storageBucket: process.env.VUE_APP_STORAGEBUCKET,
  messagingSenderId: process.env.VUE_APP_MESSAGINGSENDERID,
  appId: process.env.VUE_APP_APPId,
  measurementId: process.env.VUE_APP_MEASUREMENTID,
};

Vue.component("tinymce", tinymce);
Vue.component("datePicker", DatePicker);

Vue.use(moment);
Vue.component("multiselect", Multiselect);
Vue.use(VueRouter);
Vue.use(vco);
Vue.use(Scrollspy);
const VueScrollTo = require("vue-scrollto");
Vue.use(VueScrollTo);
Vue.config.productionTip = false;
Vue.use(BootstrapVue);
Vue.use(IconsPlugin);
Vue.use(Vuelidate);
Vue.use(VueMask);
Vue.use(require("vue-chartist"));
Vue.use(VueSweetalert2);
Vue.use(VueGoogleMaps, {
  load: {
    key: "AIzaSyAbvyBxmMbFhrzP9Z8moyYr6dCr-pzjhBE",
    libraries: "places",
  },
  installComponents: true,
});

export const bus = new Vue();
new Vue({
  sockets: {
    connect: function () {
      console.log("socket connected");
    },

    disconnect() {
      console.log("socket disconnected");
    },
  },
  methods: {
    clickButton: function (data) {
      // $socket is socket.io-client instance
      this.$socket.emit("emit_method", data);
    },
  },
  router,
  store,
  i18n,
  render: (h) => h(App),
}).$mount("#app");
