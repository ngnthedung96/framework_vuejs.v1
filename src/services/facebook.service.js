const facebookAppId = process.env.VUE_APP_FACEBOOK_APP_ID;
const facebookService = () => {
  window.fbAsyncInit = function () {
    FB.init({
      appId: facebookAppId,
      cookie: true,
      xfbml: true,
      status: true,
      version: "v17.0",
    });

    FB.AppEvents.logPageView();
  };

  (function (d, s, id) {
    var js,
      fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {
      return;
    }
    js = d.createElement(s);
    js.id = id;
    js.src = "https://connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  })(document, "script", "facebook-jssdk");
};
export default facebookService;
