import Vue from "vue";
import io from "socket.io";
import VueSocketIO from "vue-socket.io";

// export const SocketInstance = io("http://localhost:5555/");
export const SocketInstance = io("https://facebook-api.topmove.vn/");

Vue.use(VueSocketIO, SocketInstance);
